import os
import json

settings = None


def load_settings():
    global settings
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           'settings.json')) as settingsFile:
        settings = json.load(settingsFile)

# Llamados a la funcion load_settings()
# Cargamos el archivo settings.json para que se puedan consumir sus datos.


load_settings()
