from configurations.config import settings
from framework.WebFramework import webFramework


class LoginPage:

    instance = None

    # Constructor
    def __init__(self):
        self.driver = webFramework.get_driver()

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = LoginPage()
        return cls.instance

    # Metodo para insertar credenciales
    def insert_credentials(self):
        self.driver.find_element_by_xpath('//*[@id="txtUsername"]').send_keys(str(settings['username']))
        self.driver.find_element_by_xpath('//*[@id="txtPassword"]').send_keys(str(settings['password']))

    def insert_wrong_credentials(self):
        self.driver.find_element_by_xpath('//*[@id="txtUsername"]').send_keys(str(settings['wrong_username']))
        self.driver.find_element_by_xpath('//*[@id="txtPassword"]').send_keys(str(settings['wrong_password']))

    # Verificar el mensaje de error con credenciales incorrectas
    def verify_error_credentials(self):
        assert 'Invalid credentials' in self.driver.find_element_by_xpath('//*[@id="spanMessage"]').text, \
        f'Error: Mensaje no encontrado en {self.driver.current_url}'

    def click_login_button(self):
        self.driver.find_element_by_xpath('//*[@id="btnLogin"]').click()


loginPage = LoginPage.get_instance()
