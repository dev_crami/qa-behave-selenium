import time
from behave import given, when, then
from framework.WebFramework import webFramework
from pages.LoginPage import loginPage
from configurations.config import settings
from utilities.Logger import Logger

# Generamos el Logger
logger = Logger.log_generator()

# Implementamos el Given


@given(u'El Usuario navega a login')
def navegar_login(context):
    logger.info('---- Navegador Carga y Navega a Login ------')
    webFramework.load_website()
    logger.info('---- Navegador Carga y Navega a Login Page ----')


@when(u'El usuario introduce sus credenciales')
def introducir_credenciales(context):
    logger.info('--- Usuario introduce sus credenciales ----')
    loginPage.insert_credentials()
    logger.info('--- Usuario introduce sus credenciales correctamente ----')


@when(u'El usuario pulsa el boton de login')
def pulsar_boton_login(context):
    logger.info('---- Usuario pulsa boton de login')
    loginPage.click_login_button()
    logger.info('---- Usuario pulsa el boton de Login Correctamente')


@then(u'Cerramos el navegador')
def cerrar_navegador():
    webFramework.close_browser()


@then(u'Se muestra un mensaje "Invalid Credentials" en la pagina')
def mostrar_mensaje_error():
    time.sleep(2)
    loginPage.verify_error_credentials()


