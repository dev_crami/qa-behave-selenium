import webbrowser

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.utils import ChromeType
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options
from urllib.parse import urljoin

# Importamos las configuraciones de nuestro paquete configurations
from configurations.config import settings


# Creamos un framework que contendrá la configuración del WebDriver de Selenium
class WebFramework:

    instance = None

    # Constructor de la clase
    def __init__(self):
        # Comprobamos los settings y vemos el navegador configurado
        if str(settings['browser']).lower() == 'chrome':
            chrome_options = Options()
            chrome_options.add_argument('--headless')
            # self.driver = webdriver.Chrome(ChromeDriverManager(chrome_type=ChromeType.GOOGLE).install())
            self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='/usr/local/bin')
            self.driver.implicitly_wait(30)
            self.driver.set_page_load_timeout(30)
            self.driver.maximize_window()

        elif str(settings['browser']).lower() == 'firefox':
            firefox_options = Options()
            firefox_options.add_argument('--headless')
            self.driver = webdriver.Firefox(GeckoDriverManager().install())
            self.driver.implicitly_wait(30)
            self.driver.set_page_load_timeout(30)
            self.driver.maximize_window()

        else:
            chrome_options = Options()
            chrome_options.add_argument('--headless')
            self.driver = webdriver.Chrome(ChromeDriverManager().install())
            self.driver.implicitly_wait(30)
            self.driver.set_page_load_timeout(30)
            self.driver.maximize_window()

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = WebFramework()
        return cls.instance

    def get_driver(self):
        return self.driver

    # Métodos para interactuar con la página web

    # la URL sale de los settings.json
    def load_website(self):
        self.driver.get(settings['url'])

    # Navegar a otra URL que no sea la de por defecto
    def navigate_page(self, endpoint: str):
        self.driver.get(urljoin(settings['url'], endpoint))

    # Cerrar el navegador
    def close_browser(self):
        self.driver.close()

    # Verificar que un elemento existe en la página, por etiqueta
    def verify_component_by_tag(self, component):
        assert component in self.driver.find_element_by_tag_name('body').text, \
            f'Componente { component } no existe en la página actual: {self.driver.current_url}'


# Inicializamos el WebFramework
# A partir de esta línea, podemos hacer uso de WebFramework y sus métodos
# a lo largo del proyecto
webFramework = WebFramework.get_instance()
