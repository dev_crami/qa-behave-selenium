import logging
import logging.handlers

"""
Clase Logger que nos sirve para registrar eventos en un archivo llamado
tests.log dentro de la carpeta logs."""


class Logger:

    @staticmethod
    def log_generator():

        # Configuracion basica al Logger para saber donde debe alojar tests.log
        # El formato, el modo escritura (w) y el formato de fecha (dia/mes/año hora:minuto:segundo).

        logging.basicConfig(
            filename="logs/tests.log",
            filemode='W',
            format='%(acttime)s - %(message)s',
            datefmt='%d/%b/%y %H:%M:%S'
        )

        # Definimos cuantos archivos vamos a mantener y el tamaño.
        r_file = logging.handlers.RotatingFileHandler(
            'logs/tests.log',
            backupCount=3,
            maxBytes=1024 * 1024 * 20
        )

        # Finalmente se define el Logger que vamos a obtener y a usar a lo largo del test
        logger = logging.getLogger()
        logger.addHandler(r_file)
        logger.setLevel(logging.INFO)

        return logger
