# Feature para hacer un login correcto
  @login
  Feature: Ejemplo de login de la pagina OrangeHRM

    Background: Navegar a login
      Given El Usuario navega a login

    Scenario: Login correcto
      When El usuario introduce sus credenciales
      And El usuario pulsa el boton de login
      Then Cerramos el navegador

    Scenario: Login incorrecto
      When El usuario introduce sus credenciales erroneas
      And El usuario pulsa el boton de login
      Then Se muestra un mensaje "Invalid Credentials" en la pagina
      And Cerrar el navegador
